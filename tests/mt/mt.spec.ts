import { ExpressApp, IHttp } from "../../src/ExpressApp";
import { expect } from "chai";
import supertest from "supertest";
import { WinstonLogger } from "../../src/WinstonLogger";
import { LoggingLevel } from "../../src/ILogger";
import { ContractsController } from "../../src/controllers/ContractsController";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "mt" });

describe("Module Tests", function () {
    it("given wrong route then shall return 404", async () => {
        const expressApp = new ExpressApp(logger, 3001);

        const contractControllers = new ContractsController(logger);
        expressApp.addController(contractControllers);

        const result: any = await expressApp.supertest().get("/ups");
        expect(result.status).equals(404);
    })
    it("given no profile the shall return 401", async () => {
        const expressApp = new ExpressApp(logger, 3001);

        const contractControllers = new ContractsController(logger);
        expressApp.addController(contractControllers);

        const result: any = await expressApp.supertest().get("/contracts/1");
        expect(result.status).equals(401);
    })
    it("given existing profile and contract then shall return 200", async () => {
        const expressApp = new ExpressApp(logger, 3001);
        
        const contractControllers = new ContractsController(logger);
        expressApp.addController(contractControllers);

        const result: any = await expressApp.supertest()
            .get("/contracts/1")
            .set("profile_id", "1")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        console.log(JSON.stringify(result));
    })
});