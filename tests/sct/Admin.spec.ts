import { LoggingLevel } from "../../src/ILogger";
import { HttpRequest } from "../../src/HttpRequest";
import { WinstonLogger } from "../../src/WinstonLogger";
import { expect } from "chai";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sct" });

const API_SERVER_PORT = process.env.API_SERVER_PORT ? process.env.API_SERVER_PORT : 3001;
const API_SERVER_HOST = process.env.API_SERVER_HOST ? process.env.API_SERVER_HOST : "http://localhost";
const HOST_URL = API_SERVER_HOST + ":" + API_SERVER_PORT;

describe("System Component Tests", function () {
    describe("Admin", function () {
        it("shall pass", async () => {
            const startDate = new Date()
            const request = new HttpRequest(logger);
            const options = { path: `/admin/best-profession?start=2019-08-10T00:00:00.000Z&end=2100-08-10T00:00:00.000Z`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
            expect(resp.body.profession).equals("Programmer");
        });
        it("shall pass", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/admin/best-clients?start=2019-08-10T00:00:00.000Z&end=2100-08-10T00:00:00.000Z&limit=3`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
            expect(resp.body).has.lengthOf(3);
            expect(resp.body[0].profileProfession).equals("Pokemon master");
        });
    });
});