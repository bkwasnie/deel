import { LoggingLevel } from "../../src/ILogger";
import { HttpRequest } from "../../src/HttpRequest";
import { WinstonLogger } from "../../src/WinstonLogger";
import { expect } from "chai";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sct" });

const API_SERVER_PORT = process.env.API_SERVER_PORT ? process.env.API_SERVER_PORT : 3001;
const API_SERVER_HOST = process.env.API_SERVER_HOST ? process.env.API_SERVER_HOST : "http://localhost";
const HOST_URL = API_SERVER_HOST + ":" + API_SERVER_PORT;

describe("System Component Tests", function () {
    describe("Jobs", function () {
        it("given existing profile 1 when retrieving unpaid jobs then only unpaid and user jobs shall be returned", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/jobs/unpaid`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
            expect(resp.body).has.lengthOf(1);
        });
        it("given existing profile 7 when retrieving unpaid jobs then only unpaid and user jobs shall be returned", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/jobs/unpaid`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 7 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
            expect(resp.body).has.lengthOf(2);
        });
        it("given existing profile 7 (contractor) when paying for a job then shall return 500", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/jobs/5/pay`, method: "POST", headers: { "Content-Type": "application/json", "profile_id": 7 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(500);
        });
        it("given existing profile 4 (client) when paying for a job with no money then shall return 500", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/jobs/5/pay`, method: "POST", headers: { "Content-Type": "application/json", "profile_id": 4 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(500);
            expect(resp.statusMessage).equals("Internal Server Error: You don't have money to pay");
        });
        it("given existing profile 1 (client) when paying for a job with enough money then shall balance", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/jobs/2/pay`, method: "POST", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
        });
    });
});