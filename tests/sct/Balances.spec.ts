import { LoggingLevel } from "../../src/ILogger";
import { HttpRequest } from "../../src/HttpRequest";
import { WinstonLogger } from "../../src/WinstonLogger";
import { expect } from "chai";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sct" });

const API_SERVER_PORT = process.env.API_SERVER_PORT ? process.env.API_SERVER_PORT : 3001;
const API_SERVER_HOST = process.env.API_SERVER_HOST ? process.env.API_SERVER_HOST : "http://localhost";
const HOST_URL = API_SERVER_HOST + ":" + API_SERVER_PORT;

describe("System Component Tests", function () {
    describe("Balances", function () {
        it("shall not pass", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/balances/deposit/2`, method: "POST", headers: { "Content-Type": "application/json", "profile_id": 2 } };
            const resp = await request.request(HOST_URL, options, { amount: 100000 });
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(500);
            expect(resp.statusMessage).equals("Inetrnal Server Error: Can't deposit so much");
        });
        it("shall pass", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/balances/deposit/2`, method: "POST", headers: { "Content-Type": "application/json", "profile_id": 2 } };
            const resp = await request.request(HOST_URL, options, { amount: 100 });
            console.log(JSON.stringify(resp, null, 4));
            expect(resp.statusCode).equals(200);
        });
    });
});