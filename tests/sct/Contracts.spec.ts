import { LoggingLevel } from "../../src/ILogger";
import { HttpRequest } from "../../src/HttpRequest";
import { WinstonLogger } from "../../src/WinstonLogger";
import { expect } from "chai";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sct" });

const API_SERVER_PORT = process.env.API_SERVER_PORT ? process.env.API_SERVER_PORT : 3001;
const API_SERVER_HOST = process.env.API_SERVER_HOST ? process.env.API_SERVER_HOST : "http://localhost";
const HOST_URL = API_SERVER_HOST + ":" + API_SERVER_PORT;

describe("System Component Tests", function () {
    describe("Contracts", function () {
        it("given no profile then 401 shall return", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts/1`, method: "GET", headers: { "Content-Type": "application/json" } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(401);
        });
        it("given existing profile and wrong contract then 404 shall return", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts/999`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(404);
        });
        it("given existing profile and contract then 200 shall return", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts/1`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(200);
        });
        it("given existing profile and contract then 200 shall return", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts/2`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(200);
        });
        it("given existing profile and contract then 200 shall return", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts/3`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(403);
        });
        it("given existing profile when retrieving contracts then only none terminated shall be returned", async () => {
            const request = new HttpRequest(logger);
            const options = { path: `/contracts`, method: "GET", headers: { "Content-Type": "application/json", "profile_id": 1 } };
            const resp = await request.request(HOST_URL, options);
            expect(resp.statusCode).equals(200);
            expect(resp.body).has.lengthOf(1);
        });
    });
});