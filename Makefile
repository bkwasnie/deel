GIT_COMMIT := $(shell git rev-parse --short HEAD)
SERVICE_NAME := deel-api

ifeq ($(CI_COMMIT_REF_SLUG),)
  GIT_BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
else
  GIT_BRANCH=$(CI_COMMIT_REF_SLUG)
endif

ifneq (,$(wildcard ./config.env))
	include config.env
	export
endif

ifneq (,$(wildcard ./.env))
	include .env
	export
	ENV_FILE_PARAM = --env-file ./.env
endif