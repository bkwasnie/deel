import { HttpServer } from "./HttpServer";
import { LoggingLevel } from "./ILogger";
import { WinstonLogger } from "./WinstonLogger";

const API_SERVER_PORT = process.env.API_SERVER_PORT ? process.env.API_SERVER_PORT : 3001;

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sct" });
const httpServer = new HttpServer(logger, { port: API_SERVER_PORT });

init(httpServer);

async function init(httpServer: HttpServer) {
  try {
    httpServer.start();
  } catch (error) {
    console.error(`An error occurred: ${JSON.stringify(error)}`);
    process.exit(1);
  }
}