import http from "http";
import { ILogger } from "./ILogger";

export class HttpRequest {
  public constructor(private logger: ILogger) {}

  public get(url: string): Promise<any> {
    return this.request(url, { method: "GET", headers: {} }).then((resp) => {
      return resp.body;
    });
  }

  public request(url: string, options: http.RequestOptions, data?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const req = http.request(url, options, (res) => {
        let raw = "";
        const chunks: any = [];

        res.on("data", (chunk: string) => {
          raw += chunk;
          chunks.push(chunk)
        });

        res.on("end", () => {
          return resolve({
            statusCode: res.statusCode,
            statusMessage: res.statusMessage,
            headers: res.headers,
            body: raw ? this.parseResponseBody(res.headers, raw, chunks) : undefined,
            method: res.method,
            url: res.url,
          });
        });

        res.on("error", (e) => {
          reject(e);
        });

      });

      req.on("error", (e) => {
        reject(e);
      });

      if (data) {
        const dataTowrite = this.parseRequestBody(options.headers, data);
        req.write(dataTowrite);
      }

      req.end();
    });
  }

  private parseRequestBody(reqHeaders: any, data: string | any) {
    if (reqHeaders) {
      const contentType = reqHeaders["content-type"] ? reqHeaders["content-type"] : reqHeaders["Content-Type"];
      const transferEncoding = reqHeaders["transfer-encoding"] ? reqHeaders["transfer-encoding"] : reqHeaders["Transfer-Encoding"];

      if (contentType && contentType.indexOf("application/json") > -1) {
        const body = JSON.stringify(data);
        if (!transferEncoding) reqHeaders["Content-Length"] = body.length;
        return body;
      }
    }
    return data;
  }

  private parseResponseBody(resHeaders: any, raw: string, chunks: any) {
    if (resHeaders) {
      const contentType = resHeaders["content-type"] ? resHeaders["content-type"] : resHeaders["Content-Type"];
      if (!contentType || contentType.indexOf("application/json") > -1) {
        try {
          return JSON.parse(raw);
        } catch (err) {
          return raw;
        }
      } else {
        return Buffer.concat(chunks);
      }
    }
    return raw;
  }
}
