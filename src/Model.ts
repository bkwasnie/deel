import { Sequelize, Options, Model, DataTypes, Op, QueryTypes } from "sequelize";
import { LoggingLevel } from "./ILogger";
import { WinstonLogger } from "./WinstonLogger";

const logger = new WinstonLogger({ level: LoggingLevel.debug, module: "sequelize" });

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite3',
  logging: (msg: any) => logger.debug("SQL", msg)
});

class Profile extends Model {
  public firstName?: string;
  public lastName?: string;
  public profession?: string;
  public balance?: number;
  public type?: string;

  public static async GetById(id: number) {
    return Profile.findByPk(id);
  }
  
  public static async GetBestProffesion(start: string, end: string) {

    const sqlAll = `
      SELECT
        Jobs.id as jobId,
        Jobs.price as jobPrice,
        Jobs.paid as jobPaid,
        Jobs.paymentDate as jobPaymentDate,
        Contracts.id as contractId,
        Profiles.id as profileId,
        Profiles.type as profileType,
        Profiles.firstName as profileFirstName,
        Profiles.lastName as profileLastName,
        Profiles.profession as profileProfession,
        Profiles.balance as profileBalance
      FROM 
        Jobs, 
        Contracts, 
        Profiles 
      WHERE 
        Jobs.ContractId=Contracts.id AND
        Contracts.ContractorId=Profiles.id AND
        Jobs.paid=1 AND
        Jobs.paymentDate>='2020-08-15 19:11:26' AND Jobs.paymentDate<='2020-08-23 19:12:26'
      ORDER BY jobPaymentDate;`;

    const sql = `
      SELECT
        SUM(Jobs.price) as priceTotal, 
        Profiles.profession as profession
      FROM 
        Jobs, 
        Contracts, 
        Profiles 
      WHERE 
        Jobs.ContractId=Contracts.id AND
        Contracts.ContractorId=Profiles.id AND
        Jobs.paid=1 AND
        Jobs.paymentDate>='${start}' AND Jobs.paymentDate<='${end}'
      GROUP BY 
        Profiles.profession
      ORDER BY priceTotal DESC;`;
    
    return sequelize.query(sql, { type: QueryTypes.SELECT}).then(results => {
      return results[0];
    });
  }

  public static async GetBestClients(start: string, end: string, limit = 2) {
    const sql = `
      SELECT
        SUM(Jobs.price) as totalPaid,
        Profiles.id as profileId,
        Profiles.type as profileType,
        Profiles.firstName as profileFirstName,
        Profiles.lastName as profileLastName,
        Profiles.profession as profileProfession,
        Profiles.balance as profileBalance
      FROM 
        Jobs, 
        Contracts, 
        Profiles 
      WHERE 
        Jobs.ContractId=Contracts.id AND
        Contracts.ClientId=Profiles.id AND
        Jobs.paymentDate>='${start}' AND Jobs.paymentDate<='${end}'
      GROUP BY 
        Profiles.id
      ORDER BY totalPaid DESC
      LIMIT ${limit};`;

    return sequelize.query(sql, { type: QueryTypes.SELECT}).then(results => {
      return results;
    });
  }

}

Profile.init(
  {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    profession: {
      type: DataTypes.STRING,
      allowNull: false
    },
    balance:{
      type: DataTypes.DECIMAL(12,2)
    },
    type: {
      type: DataTypes.ENUM('client', 'contractor')
    }
  },
  {
    sequelize,
    modelName: 'Profile'
  }
);

class Contract extends Model {
  public ClientId?: number;
  public ContractorId?: number;

  public static async GetAll() {
    return Contract.findAll();
  }
  public static async GetAllByProfileId(profileId: number, statuses: string[] = []) {
    const where: any = {
      [Op.or]: [
        { ContractorId: profileId },
        { ClientId: profileId }
      ]
    };

    if (statuses.length > 0) {
      where["status"] = {
        [Op.in]: statuses
      };
    }

    return Contract.findAll({ where });
  }
  public static async GetAllNoneTerminatedByProfileId(profileId: number) {
    return Contract.findAll({
      where: {
        [Op.or]: [
          { ContractorId: profileId },
          { ClientId: profileId }
        ],
        status: {
          [Op.ne]: "terminated"
        }
      }
    });
  }

}

Contract.init(
  {
    terms: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status:{
      type: DataTypes.ENUM('new','in_progress','terminated')
    }
  },
  {
    sequelize,
    modelName: 'Contract'
  }
);

class Job extends Model {
  public description?: string;
  public paid?: boolean;
  public price?: number;
  public paymentDate?: Date;
  public Contract?: Contract;

  public static async GetAllUnpaidByProfileIdAndStatus(profileId: number, statuses: string[]) {
    return Job.findAll({
      where: {
        paid: false,
        "$Contract.status$": {
          [Op.in]: statuses
        },
        [Op.or]: [
          { "$Contract.ContractorId$": profileId },
          { "$Contract.ClientId$": profileId }
        ],
      },
      include: [
        {
          model: Contract,
          attributes: ["status", "ContractorId", "ClientId"]
        }
      ]
    });
  }
  public static async GetWithContract(id: number) {
    return Job.findByPk(id, {
      include: [
        {
          model: Contract,
          attributes: ["status", "ContractorId", "ClientId"]
        }
      ]
    });
  }
}

Job.init(
  {
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    price:{
      type: DataTypes.DECIMAL(12,2),
      allowNull: false
    },
    paid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    paymentDate:{
      type: DataTypes.DATE
    }
  },
  {
    sequelize,
    modelName: 'Job'
  }
);

Profile.hasMany(Contract, {as :'Contractor',foreignKey:'ContractorId'})
Contract.belongsTo(Profile, {as: 'Contractor'})

Profile.hasMany(Contract, {as : 'Client', foreignKey:'ClientId'})
Contract.belongsTo(Profile, {as: 'Client'})

Contract.hasMany(Job)
Job.belongsTo(Contract)

export {
  sequelize,
  Profile,
  Contract,
  Job
};