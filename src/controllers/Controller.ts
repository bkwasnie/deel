import { Router } from "express";
import { ILogger } from "../ILogger";
import { Authenticator } from "../middleware/Authenticator";

export abstract class Controller {
    protected expressRouter!: Router;
    public abstract path(): string;
    public constructor(protected logger: ILogger, protected authenticator: Authenticator = new Authenticator(logger)) {
        this.expressRouter = Router();
    }
    public router(): Router {
        return this.expressRouter;
    }
    protected requestInfo(req: any) {
        return { request: { profile_id: req.headers.profile_id, query: req.query, path: req.path, params: req.params, body: req.body, profile: req.profile } };
    }
}