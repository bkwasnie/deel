import { Controller } from "./Controller";
import { Request, RequestHandler, Response, Router } from "express";
import { ILogger } from "../ILogger";
import { Contract, Job, Profile, sequelize } from "../Model";
import { Authenticator } from "../middleware/Authenticator";
import { cursorTo } from "readline";


const MAX_DEPOSIT_RATIO = 0.25;

export class BalancesController extends Controller {
    public constructor(logger: ILogger, authenticator?: Authenticator) {
        super(logger, authenticator);
        this.logger.info("BalancesController", "creating controller with express router");

        this.expressRouter = Router();
        this.expressRouter.post('/deposit/:userId', this.authenticator.byProfile, this.deposit);
    }
    public path(): string {
        return "/balances"
    }
    private deposit = async (req: any, res: any) => {
        this.logger.info("ContractsController", "new request", this.requestInfo(req));
        const profile: any = req.profile;
        console.log(JSON.stringify(profile, null, 4));

        const userId = Number(req.params.userId);

        console.log(userId, profile.id);

        if (userId !== profile.id) {
            res.status(401).end();
            return;
        }

        const jobs = await Job.GetAllUnpaidByProfileIdAndStatus(profile.id, ["new", "in_progress"]);
        console.log(JSON.stringify(jobs, null, 4));

        const totalUnpaid = jobs.reduce((prev, cur) => prev + cur.price!, 0);
        console.log(totalUnpaid);
        const maxDeposit = MAX_DEPOSIT_RATIO * totalUnpaid;
        console.log(maxDeposit);

        if (req.body.amount > maxDeposit) {
            res.statusMessage = "Inetrnal Server Error: Can't deposit so much";
            res.status(500).end();
            return;
        }

        await profile.update({
            balance: profile.balance + req.body.amount
        });

        res.json(profile);
    }
}