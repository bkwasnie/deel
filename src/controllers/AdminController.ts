import { Controller } from "./Controller";
import { Request, RequestHandler, Response, Router } from "express";
import { ILogger } from "../ILogger";
import { Contract, Job, Profile, sequelize } from "../Model";
import { Authenticator } from "../middleware/Authenticator";

export class AdminController extends Controller {
    public constructor(logger: ILogger, authenticator?: Authenticator) {
        super(logger, authenticator);
        this.logger.info("AdminController", "creating controller with express router");

        this.expressRouter = Router();
        this.expressRouter.get('/best-profession', this.authenticator.byProfile, this.bestProffesion);
        this.expressRouter.get('/best-clients', this.authenticator.byProfile, this.bestClients);
    }
    public path(): string {
        return "/admin"
    }
    private bestProffesion = async (req: any, res: any) => {
        this.logger.info("AdminController::bestProffesion()", "new request", this.requestInfo(req));
        const start = req.query.start;
        const end = req.query.end;
        const result = await Profile.GetBestProffesion(start, end);
        res.json(result);
    }
    private bestClients = async (req: any, res: any) => {
        this.logger.info("AdminController::bestClients()", "new request", this.requestInfo(req));
        const start = req.query.start;
        const end = req.query.end;
        const limit = req.query.limit;
        const result = await Profile.GetBestClients(start, end, limit);
        res.json(result);
    }

}