import { Controller } from "./Controller";
import { Request, RequestHandler, Response, Router } from "express";
import { ILogger } from "../ILogger";
import { Authenticator } from "../middleware/Authenticator";
import { Contract, Profile, sequelize } from "../Model";

export class ContractsController extends Controller {
    public constructor(logger: ILogger, authenticator?: Authenticator) {
        super(logger, authenticator);
        this.logger.info("ContractsController", "creating controller with express router");

        this.expressRouter = Router();
        this.expressRouter.get('/:id', this.authenticator.byProfile, this.getById);
        this.expressRouter.get('/', this.authenticator.byProfile, this.getAll);
    }
    public path(): string {
        return "/contracts"
    }

    private getById = async (req: any, res: any) =>{
        this.logger.info("ContractsController", "new request", this.requestInfo(req))
        const profile: any = req.profile;

        const {Contract} = req.app.get('models')
        const {id} = req.params

        const contract = await Contract.findOne({where: {id}})
        if(!contract) return res.status(404).end()

        this.logger.info("ContractsController", "contract found", { contract });

        if (contract.ContractorId === profile.id) {
            this.logger.info("ContractsController", "contractor contract found", { contract });
            res.json(contract);
            return;
        }

        if (contract.ClientId === profile.id) {
            this.logger.info("ContractsController", "client contract found", { contract });
            res.json(contract);
            return;
        }

        this.logger.error("ContractsController", "access to contract forbidden, contract does not belong to profile");
        res.status(403).end();
    }
    private getAll = async (req: any, res: any) => {
        const profile: any = req.profile;
        const contracts = await Contract.GetAllNoneTerminatedByProfileId(profile.id);
        res.json(contracts);
    }
}