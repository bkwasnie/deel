import { Controller } from "./Controller";
import { Request, RequestHandler, Response, Router } from "express";
import { ILogger } from "../ILogger";
import { Contract, Job, Profile, sequelize } from "../Model";
import { Authenticator } from "../middleware/Authenticator";

export class JobsController extends Controller {
    public constructor(logger: ILogger, authenticator?: Authenticator) {
        super(logger, authenticator);
        this.logger.info("JobsController", "creating controller with express router");

        this.expressRouter = Router();
        this.expressRouter.get('/unpaid', this.authenticator.byProfile, this.getUnpaid);
        this.expressRouter.post('/:job_id/pay', this.authenticator.byProfile, this.pay);
    }
    public path(): string {
        return "/jobs"
    }
    // for ***active contracts only*** - my understanding is the in_progress only (none terminated and none new), hmm ...
    private getUnpaid = async (req: any, res: any) => {
        this.logger.info("JobsController::getUnpaid()", "new request");

        const profile: any = req.profile;

        const jobs = await Job.GetAllUnpaidByProfileIdAndStatus(profile.id, ["in_progress"]);
        res.json(jobs);
    }

    private pay = async (req: any, res: any) => {
        this.logger.info("JobsController::payContractor()", "new request", this.requestInfo(req))
        const profile: any = req.profile;

        console.log(JSON.stringify(profile, null, 4))

        if (profile.type !== "client") {
            this.logger.error("JobsController:pay()", "You are not a client of this job");
            res.statusMessage = "Internal Server Error: You are not a client of this job";
            return res.status(500).end();
        }

        const { job_id } = req.params;

        const job = await Job.GetWithContract(job_id);

        if(!job) return res.status(404).end()

        console.log(JSON.stringify(job, null, 4));

        if (job.paid === true) {
            this.logger.error("JobsController:pay()", "Job already paid");
            return res.status(500).end();
        }

        if (job.Contract?.ClientId !== req.profile.id) {
            this.logger.error("JobsController:pay()", "You are not a client of this job");
            res.statusMessage = "Internal Server Error: You are not a client of this job";
            return res.status(500).end();
        }

        const contractor = await Profile.GetById(job.Contract!.ContractorId!);
        if(!contractor) {
            res.statusMessage = "Internal Server Error: Missing contractor";
            return res.status(500).end();
        }
        console.log(JSON.stringify(contractor, null, 4));

        if (profile.balance < job.price!) {
            this.logger.error("JobsController:pay()", "You don't have money");
            res.statusMessage = "Internal Server Error: You don't have money to pay";
            return res.status(500).end();
        }

        const updateClient = profile?.update({
            balance: profile.balance! - job.price!
        });

        const updateContractor = contractor?.update({
            balance: contractor.balance! + job.price!
        });

        const updateJob = job?.update({
            paid: true,
            paymentDate: new Date()
        });

        // if all jobs are paid =? then contract should be terminated ????
        // const updateContract = job.Contract?.update({
        //     paid: true,
        //     paymentDate: new Date()
        // });

        await Promise.all([updateClient, updateContractor, updateJob]);

        res.status(200).json(await Job.GetWithContract(job_id));
    }
}