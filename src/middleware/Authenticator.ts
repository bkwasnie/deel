import { ILogger } from "../ILogger";

export class Authenticator {
    public constructor(private logger: ILogger) {}
    public byProfile = async (req: any, res: any, next: any) => {
        this.logger.info("Authenticate::byProfile()", "authenticating");
        const { Profile } = req.app.get('models');
        
        const profile = await Profile.findOne({
            where: {
                id: req.get('profile_id') || 0
            }
        });
        
        if(!profile) {
            this.logger.error("Authenticate::byProfile()", "missing profile");
            return res.status(401).end();
        }
        
        req.profile = profile;
        next();
    }
}