import { AdminController } from "./controllers/AdminController";
import { BalancesController } from "./controllers/BalancesController";
import { ContractsController } from "./controllers/ContractsController";
import { JobsController } from "./controllers/JobsController";
import { ExpressApp } from "./ExpressApp";
import { ILogger, LoggingLevel } from "./ILogger";
import { WinstonLogger } from "./WinstonLogger";

export class HttpServer {
  private expressApp: ExpressApp;
  public constructor(private logger: ILogger, private config: any) {
    this.expressApp = new ExpressApp(this.logger, config.port);
    
    const contractController = new ContractsController(this.logger);
    const jobsController = new JobsController(this.logger);
    const balancesController = new BalancesController(this.logger);
    const adminController = new AdminController(this.logger);

    this.expressApp.addController(contractController);
    this.expressApp.addController(jobsController);
    this.expressApp.addController(balancesController);
    this.expressApp.addController(adminController);
  }
  public start() {
    this.expressApp.listen(() => {
      console.log(`Http Server Listening on Port: ${this.config.port}`);
    });
  }
}