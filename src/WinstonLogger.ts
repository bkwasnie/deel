import { ILogger, ILoggerConfig, LoggingLevel } from "./ILogger";
import * as Winston from "winston";
import {} from "winston";
import { info } from "console";

export class WinstonLogger implements ILogger {
  private logger: Winston.Logger;

  public constructor(private config: ILoggerConfig) {
    const transports: Winston.transport[] = [new Winston.transports.Console()];

    this.logger = Winston.createLogger({
      level: LoggingLevel[config.level],
      defaultMeta: { module: this.config.module },
      format: Winston.format.printf(this.formatCustom),
      transports,
    });
  }

  public info(where: string, message?: string, metadata?: any): void {
    this.logger.info(message || "", { where, metadata });
  }

  public warn(where: string, message?: string, metadata?: any): void {
    this.logger.warn(message || "", { where, metadata });
  }

  public error(where: string, message?: string, metadata?: any): void {
    this.logger.error(message || "", { where, metadata });
  }

  public debug(where: string, message?: string, metadata?: any): void {
    this.logger.debug(message || "", { where, metadata });
  }

  private formatRaw = (info: any) => `${JSON.stringify(info)}`;
  private formatCustom = (info: any) => `${JSON.stringify({
    timestamp: new Date().toISOString(),
    level: info.level, 
    module: info.module + ":" + info.where, 
    message: info.message,
    ...info.metadata
  })}`;

}
