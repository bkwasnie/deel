import * as express from "express";
import { Application, ErrorRequestHandler, RequestHandler } from "express";
import { Profile, sequelize } from "./Model";
import bodyParser from "body-parser";
import { Authenticator } from "./middleware/Authenticator";
import Http from "http";
import supertest from "supertest";
import { ILogger } from "./ILogger";
import { Controller } from "./controllers/Controller";

export interface IHttp {
    createServer(options: Http.ServerOptions, application: Application): Http.Server;
}


export class ExpressApp {
    private app: Application;
    private server: Http.Server;
    public constructor(private logger: ILogger, private port: number, private http: any = Http) {
        this.app = express.default();
        this.server = this.http.createServer(this.app);

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.set("sequelize", sequelize)
        this.app.set("models", sequelize.models)
    }

    public addController(controller: Controller) {
        this.app.use(controller.path(), controller.router());
    }

    public supertest() {
        return supertest(this.app);
    }

    public listen(callback: () => void) {
        this.server.listen(this.port, callback);
    }
}